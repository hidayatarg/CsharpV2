﻿# Markdown file






For Loops
    for(var i=0;i<10;i++)
    {
    //codes here
    }

Foreach Loops
    foreach (var number in numbers)
    {
    //codes here
    }

While Loops 
    while (i<10)
    {
    //codes
    i++;
    }

Do while Loops
    do
    {
    //codes
    i++;
    }while (i<10);



Break: Jumps out of the loop
Continue: Jumps to the next iteration



There is no really right or wrong code you need to make your own judgement 
of which code is right and which code is wrong.

Your code should be readable.


		
		Arrays: Refresent a fixed number of a variable of a particular type
single dimension

var number =new int [5];

var number =new int [5]{1,2,1,1,1};

Multi dimension
*Rectangular 3X5
var matrices= new int [3,5]
object intilzation syntax
var matrices= new int [3,5]
{
{1,2},
{1,5}
};

acessing an element in Array
var element=matrix[0,0];
*jagged row and column different
var array = new int [3][]

//row      //contains 
array[0]=new int[4]; //size foru
array[1]=new int[3];
array[2]=new int[2];

NOTE: Once you created an array you cant change but there ways to work with them.

LIST
a number of object you work you are not sure how many you wil work with 
in C# we use a list when we dont know how many object we will store in it.
an array: fixedSize
List dynamic:size
	var number= new List<int>();
		var number= new List<int>(){1,2,5,};

		useful method applied 
		*add()
		* addRange()
		*Remove()
		*RemoveAt()
		*IndexOf()
		*Contains()
		*Count
		

		arrays are fixed in size while list are dynamics more can be added later

		99percent list are used but may be using a third party library and legacy so you may use array but prefer list ...

		String
		Is related to a class in .net library
		it is immutable it means once you created them you cant change

		Formatting
		ToLower 
		ToUpper
		Trim /to remove spaces

		Searching 
		IndexOf 
		Last IndexOf


		String Builder 
		*Defined in System.Text
		*A mutable String
		*Easy and fast to create and manupulate a string

		but not optimized for Searching
		doesnt give indexof last IndexOf Contains startwith


		string manupulation
		Append() to add at the end of the String
		Insert () to add at the given index of the string
		remove something from String
		replace to replace from string
		clear to clear from string



		Procedural Programming : a Programming paradigm based on Procedure calls
		in real world application we dont write evey method in main we divide into class they program in real world may contains many methods and functionalities.

		Object-oriented Programming: 


		when we are in program.cs and we want to create a method and if we use that method from main the method needs to  be static inorder to work with it.



		WOrking with FIles 
		SYSTEM.IO
		Most used clases
		*file,fileInfo class
		*Directory, DirectoryInfo class
		*Path class


		File, FileInfo class: provide methods for copying creating deleting , moving and opening files.

		fileInfo: provides instance method 
		file: provide static method (good for small operation like get the file attributes) when computer work this it checks your priorty if working with big file will affect your performance
		Methods included
		create()
		Copy()
		Delete()
		Exits()
		GetAttribute()
		Move()
		ReadAllText()


		DirectoryInfo: provides instance method 
		Directory: provide static method

		CreateDirectory()
		Delete()
		Exits()
		GetCurrentDirectory()
		GetFiles() //retun files in directory or with specific creatira
		Move()
		GetLogicalDrives()


		Path Class
		GetDirectoryName()
		GetExtension()
		GetTempPath()

