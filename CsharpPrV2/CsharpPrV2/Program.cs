﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Text;
using System.IO;
namespace CsharpPrV2
    
{
    partial class Program
    {
        static void Main(string[] args)
        {
             //Directory
            Directory.CreateDirectory(@"C:\newFolder");

            var files = Directory.GetFiles(@"C:\", "*.sln", SearchOption.AllDirectories);
            //since gives a string
            foreach (var file in files)
            {
                Console.WriteLine(file);
            }


            var directories = Directory.GetDirectories(@"C:\newFolder\CsharpFudamentals","*.sln",SearchOption.AllDirectories);

            foreach (var directory in directories)
            {
                Console.WriteLine(directory);
            }

            if (Directory.Exists(@"C:\newFolder\CsharpFuda"))
            {
                //do something
            }

            DirectoryInfo info= new DirectoryInfo(@"C:\newFolder\CsharpFuda");
            info.GetFiles();
            info.GetDirectories();

        }
    }
}
